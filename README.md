
# Dynamic Table
User has to select the number of rows from the select menu and then click on the button. Then there is generation of the table with given rows and user has to insert the values in textbox and need to click on submit button for finding its cube.
## Running Tests

To run tests, run the following command

step 1:
```bash
  Execute the html file 
```
step 2:

```bash
 Select number of rows from the select menu 
```

step 3:
```bash
  Click on Create Table button
``` 

step 4:
```bash
  Insert values in the table
``` 
step 5:
```bash
  Click on the Find Cube button
``` 
