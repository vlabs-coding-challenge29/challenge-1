 
    function addme()
    {
        var row=document.getElementById("row").value;
        var temp="t";
        if(row!=0)
        {
            //table created 
            const tbl=document.createElement("table");
            const tblBody=document.createElement("tbody");
            
            //first row of table
            const row1=document.createElement("tr");
            
            const cell1=document.createElement("th");
            const cellText1=document.createTextNode("Enter Number");
            cell1.appendChild(cellText1);
            row1.appendChild(cell1);
            
            const cell2=document.createElement("th");
            const cellText2=document.createTextNode("It's Cube");
            cell2.appendChild(cellText2);
            row1.appendChild(cell2);

            row1.class="heading";
            tblBody.appendChild(row1);
            
            //others row of table
            for(let i=0;i<row;i++)
            {
                //next row of table
                const row=document.createElement("tr");

                for(let j=0;j<2;j++)
                {
                    const cell=document.createElement("td");
                    cell.class="talign";
                    if(j==0)
                    {
                        const t=document.createElement("input");
                        t.type="text";
                        t.id=i;
                        t.class="tmargin";
                        cell.appendChild(t);
                    }
                    else
                    {
                        cell.id=temp+i;
                }       
                    row.appendChild(cell);
                }
                tblBody.appendChild(row);
            }
            tbl.appendChild(tblBody);
            document.getElementById("demo").innerHTML=tbl.innerHTML;
            
            const bu="<input type='button' value='Find Cube' class='add' onclick='showme()'>"
            document.getElementById("sub_btn").innerHTML=bu;
        }
        else
        {
            alert("Please select correct option");
            document.getElementById("demo").innerHTML="";
            document.getElementById("sub_btn").innerHTML="";
        }
       
    
    }
   
    function showme(){
        var row=document.getElementById("row").value;
        var temp="t";
        let flag=false;

        for(let i=0;i<row;i++)
        {
            var ele=document.getElementById(i).value;
            if(ele.length==0)
            {
                flag=true;
            }
            else
            {
                ele=ele*ele*ele;
            }
            document.getElementById(temp+i).innerHTML=ele;
        }

        if(flag==true)
        {
            alert("You have not entered data in few cells");
        }
        
    }